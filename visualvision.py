'''
visual vision -- do visual stuff with opencv, visually

the source code in its entirety, for now.:
'''
import tornado.ioloop
import tornado.web

import numpy as np
import cv2 as cv

################################
### OpenCV Code goes here...
def vis(img):
    pass
################################

class VisHandler(tornado.web.RequestHandler):
    def get(self):
        # get img from somewhere
        v = vis("somepic")
        self.write("[*] vis() was called")

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, visualvison!")

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/vis", VisHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
